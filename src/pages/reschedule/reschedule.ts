import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-reschedule',
  templateUrl: 'reschedule.html',
})
export class ReschedulePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReschedulePage');
  }

  goBack(){
   this.navCtrl.pop();
 }

 open_page(page){
 this.navCtrl.push(page);
 }

}
