import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-paitentdetail',
  templateUrl: 'paitentdetail.html',
})
export class PaitentdetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaitentdetailPage');
  }

  goBack(){
   this.navCtrl.pop();
 }

  open_page(page){
  this.navCtrl.push(page);
  }

  textmodal(){
    let modal = this.modalCtrl.create('TextPage');
    modal.present();
  }

  audiomodal(){
    let modal = this.modalCtrl.create('AudiocallPage');
    modal.present();
  }

  videomodal(){
    let modal = this.modalCtrl.create('VideocallPage');
    modal.present();
  }

}
