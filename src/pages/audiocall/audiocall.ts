import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-audiocall',
  templateUrl: 'audiocall.html',
})
export class AudiocallPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AudiocallPage');
  }

  dismiss() {
   this.viewCtrl.dismiss();
 }

}
